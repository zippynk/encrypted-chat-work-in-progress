"All program contents and related files © Copyright 2013 Zippynk. All rights reserved except where stated otherwise. Much thanks is given to my dad for helping me port this to python. This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en_US."
from time import time
from time import sleep
import random
combos = [] # Encryption Key.
not_used = [] # Numbers that can still be added to the key.
for i in range(3120): # Fills not_used.
    not_used.append(i)
length = 3120 # Sets up variable for determining the length of not_used.
for i in range(37): # Repeats for all characters but space, which has a different setup (160 numbers instead of 80).
    combos.append([]) # Add an item to the list for the next character.
    number = random.randint(0,length-1) # Run the process once before to avoid bugs having to do with randint ranges being zero.
    combos[len(combos)-1].insert(0,not_used[number])
    not_used.remove(not_used[number])
    length = length - 1
    for i2 in range(79): # Run for all 78 other cycles.
        number = random.randint(0,length-1)
        combos[len(combos)-1].insert(random.randint(0,len(combos[len(combos)-1])),not_used[number])
        not_used.remove(not_used[number])
        length = length - 1
combos.append([]) # Run seperately for final cycle to fix bugs.
number = random.randint(0,length-1)
combos[len(combos)-1].insert(0,not_used[number])
not_used.remove(not_used[number])
length = length - 1
for i2 in range(158): # Run the process seperately for space.
    number = random.randint(0,length-1)
    combos[len(combos)-1].insert(random.randint(0,len(combos[len(combos)-1])),not_used[number])
    not_used.remove(not_used[number])
    length = length - 1
number = 0
combos[len(combos)-1].insert(random.randint(0,len(combos[len(combos)-1])),not_used[number])
not_used.remove(not_used[number])
length = length - 1
print combos # Print final result, Only turn on for debugging purposes.
def generate_key():
    combos = [] # Encryption Key.
    not_used = [] # Numbers that can still be added to the key.
    for i in range(3120): # Fills not_used.
        not_used.append(i)
    length = 3120 # Sets up variable for determining the length of not_used.
    for i in range(37): # Repeats for all characters but space, which has a different setup (160 numbers instead of 80).
        combos.append([]) # Add an item to the list for the next character.
        number = random.randint(0,length-1) # Run the process once before to avoid bugs having to do with randint ranges being zero.
        combos[len(combos)-1].insert(0,not_used[number])
        not_used.remove(not_used[number])
        length = length - 1
        for i2 in range(79): # Run for all 78 other cycles.
            number = random.randint(0,length-1)
            combos[len(combos)-1].insert(random.randint(0,len(combos[len(combos)-1])),not_used[number])
            not_used.remove(not_used[number])
            length = length - 1
    combos.append([]) # Run seperately for final cycle to fix bugs.
    number = random.randint(0,length-1)
    combos[len(combos)-1].insert(0,not_used[number])
    not_used.remove(not_used[number])
    length = length - 1
    for i2 in range(158): # Run the process seperately for space.
        number = random.randint(0,length-1)
        combos[len(combos)-1].insert(random.randint(0,len(combos[len(combos)-1])),not_used[number])
        not_used.remove(not_used[number])
        length = length - 1
    number = 0
    combos[len(combos)-1].insert(random.randint(0,len(combos[len(combos)-1])),not_used[number])
    not_used.remove(not_used[number])
    length = length - 1
    return combos # No longer automatically prints. Call the function and print it for debugging purposes.
def decrypt(text):
    letters = "abcdefghijklmnopqrstuvwxyz1234567890. " # For finding a number's index.
    output = ""
    for i1 in range(len(text)/4):
        i2 = 0
        while not x[i1:i1+4] in combos[i2]:
            i2 = i2 + 1
        i1 = i2=1 + 4
    return output
