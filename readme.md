# Encrypted Chat (Work In Progress)
## What is it?
A chat project. Developed by Zippynk starting in December 2013. Encrypts messages, currently only fragments of encrypting algorithm work.

## Credits
Much thanks is given to my dad for helping me port this to python.

## Legal Stuff
### Disclaimer
STILL IN PRE-PRE-ALPHA - NOT YET FULLY SECURE - DO NOT USE FOR SENSITIVE DATA! NO WARRANTY PROVIDED OF ANY SORT, EXCEPT WHERE REQUIRED BY LAW.
### Translation of disclaimer (Has no legal value)
It's important, by the way, to keep in mind that none of this is secure yet.
### License
All program contents and related files © Copyright 2013 Zippynk. All rights reserved except where stated otherwise.
This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en_US.
### Clarifications of License
Derivative works must keep all copyright notices intact, under the CC BY-NC-SA 4.0 License.
### Notes about License
I'd rather you not remix, fork, or copy until it's done (although bug reports via the issue tracker are always welcome), but If you really, really, really want/need to copy it, just follow the Creative Commons License linked to above. Thanks! (P.S. I'll probably remove this once the final version is done.)
